# German translations for konqueror package
# German translation for konqueror.
# Copyright (C) 2025 This file is copyright:
# This file is distributed under the same license as the konqueror package.
# Automatically generated, 2025.
#
msgid ""
msgstr ""
"Project-Id-Version: konqueror\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2025-02-24 00:41+0000\n"
"PO-Revision-Date: 2025-02-24 14:56+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: temporarysavedir.cpp:54
#, kde-format
msgid "Temporary Download Directory"
msgstr ""

#: temporarysavedir.cpp:60
#, kde-format
msgid "Choose Custom Download Directory..."
msgstr ""

#: temporarysavedir.cpp:61
#, kde-format
msgid ""
"Choose a custom directory to use when downloading URLs from this window, "
"temporarily overriding the default one"
msgstr ""

#: temporarysavedir.cpp:66
#, kde-format
msgctxt "@action:inmenu stop using a temporary download directory"
msgid "Stop Using Temporary Download Directory"
msgstr ""

#: temporarysavedir.cpp:67
#, kde-format
msgid ""
"Use the default download directory instead of a custom one (only for this "
"window)"
msgstr ""

#: temporarysavedir.cpp:113
#, kde-format
msgid "Temporary save directory"
msgstr ""

#: temporarysavedir.cpp:114
#, kde-format
msgid "Choose Temporary Save Directory"
msgstr ""

#. i18n: ectx: Menu (tools)
#: temporarysavedir.rc:4
#, kde-format
msgid "&Tools"
msgstr ""

#. i18n: ectx: ToolBar (extraToolBar)
#: temporarysavedir.rc:8
#, kde-format
msgid "Extra Toolbar"
msgstr ""
